FROM python:3.8-slim-buster
ARG mode
WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt && if ["$mode"="test"]; then echo 'je lance le test' pytest tests.py; fi

CMD ["python3","application.py","flask", "run"]
